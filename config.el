;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Vladimir Nestor"
      user-mail-address "r3seh@ya.ru")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "CaskaydiaCove Nerd Font" :size 14 :weight 'light)
;;       doom-variable-pitch-font (font-spec :family "CaskaydiaCove Nerd Font" :size 14  :weight 'light)
;;       doom-unicode-font (font-spec :family "CaskaydiaCove Nerd Font" :size 14 :weight 'light))
(setq doom-font (font-spec :family "CaskaydiaCove Nerd Font" :size 14 :weight 'medium)
      doom-variable-pitch-font (font-spec :family "CaskaydiaCove Nerd Font" :size 14 :weight 'medium)
      doom-unicode-font (font-spec :family "CaskaydiaCove Nerd Font" :size 14 :weight 'medium))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
; (setq doom-theme 'doom-city-lights)
(setq doom-theme 'doom-dracula)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)

;; (after! yasnippet
;;    (add-to-list +snippets-dir "./my-snippets"))

;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.
(after! org
  (setq org-todo-keywords '((sequence "TODO(t)" "STR(s)"
        "PROJ(p)" "|" "DONE(d)" "CANCELLED(c)")))
  )

;; smartparens
;; add a global filter
(sp-pair "\""  nil   :actions '(:rem autoskip insert))
(sp-pair "'"   nil   :actions '(:rem autoskip insert))
(sp-pair "\\{" "\\}" :actions '(:rem autoskip))
(sp-pair "{" "}"     :actions '(:rem autoskip))
(sp-pair "\\(" "\\)" :actions '(:rem autoskip))
(sp-pair "(" ")"     :actions '(:rem autoskip))
(sp-pair "[" "]"     :actions '(:rem autoskip))

;; Fix indetns in python
(add-hook 'python-mode-hook
      (lambda ()
        (setq indent-tabs-mode nil)
        (setq tab-width 4)
        (setq python-indent-offset 4)))

;; Make windwos unicode filenames right
(setq w32-unicode-filenames t)
(setq ctl-arrow t)

;; Set time after task done
(setq org-log-done 'time)

;; add paths to PATH
;; (setenv "PATH"
;;   (concat
;;    "C:/Users/v.nestor/Documents/progs/yara-v4.1.1-1635-win64" ";"
;;    "C:/Users/v.nestor/Documents/progs/ssdeep-2.14.1" ";"
;;    "C:/Users/v.nestor/Documents/progs/oledump_V0_0_60" ";"
;;    (getenv "PATH")
;;   )
;; )

;; Disable eldoc as it freeze a bit eshell sometimes
(global-eldoc-mode -1)

;; Keybindings
(map! :leader
      (:prefix ("r" . "region")
       :desc "Fill region" :n "f" #'fill-region
       )
      )
;; Try to set ru and en dictionary by default but failed
;; (after! flyspell
;;         (setq ispell-program-name "hunspell")
;;         (setq ispell-dictionary "ru_RU,en_US")
;;         (ispell-set-spellchecker-params)
;;         (ispell-hunspell-add-multi-dic "ru_RU,en_US")
;;         )


(use-package! dirvish)

(after! mu4e
  ;; load package to be able to capture emails for GTD
  (require 'org-mu4e)
  ;; do not use rich text emails
  ; (remove-hook! 'mu4e-compose-mode-hook #'org-mu4e-compose-org-mode)
  ; (setq gnus-dired-mail-mode 'mu4e-user-agent)

  (setq mu4e-update-interval (* 3 60) ; update interval
        mu4e-confirm-quit nil ; quit without asking
        mu4e-attachment-dir "~/Downloads"
        ; mu4e-maildir (expand-file-name "~/Maildir/yawolf")
        mu4e-get-mail-command "offlineimap"
        ; mu4e-user-mail-address-list '("w.wolf.0@yandex.ru")
	;    user-mail-address "w.wolf.0@yandex.ru"
	    user-full-name "Vladimir Nestor")
  ;(setq message-send-mail-function 'smtpmail-send-it
  ;      smtpmail-stream-type 'ssl
  ;      smtpmail-smtp-user "w.wolf.0@yandex.ru"
  ;      smtpmail-smtp-server "smtp.yandex.ru"
  ;      smtpmail-smtp-service 465)
  )
(set-email-account!
 "Private" ;; Account label
 ;; Mu4e folders
 '((mu4e-sent-folder             . "/yar3seh/Отправленные")
   (mu4e-drafts-folder           . "/yar3seh/Черновики")
   (mu4e-trash-folder            . "/yar3seh/Удаленные")
   (mu4e-refile-folder           . "/yar3seh/Архив")

   ;; Org-msg template (signature and greeting)
   (org-msg-greeting-fmt         . "Добрый день%s,")
   (org-msg-signature            . "

#+begin_signature
С уважением, \\\\
Нестор Владимир
#+end_signature")

   ;; 'smtpmail' options, no need for these when using 'msmtp'
   (smtpmail-smtp-user           . "r3seh@yandex.ru")
   (smtpmail-smtp-server         . "smtp.yandex.ru")
   (smtpmail-stream-type         . ssl)
   (smtpmail-smtp-service        . 465)

   ;; By default, `smtpmail' will try to send mails without authentication, and if rejected,
   ;; it tries to send credentials. This behavior broke my configuration. So I set this
   ;; variable to tell 'smtpmail' to require authentication for our server (using a regex).
   ; (smtpmail-servers-requiring-authorization . "smtps\\.server\\.com"))
   )

 t) ;; Use as default/fallback account
